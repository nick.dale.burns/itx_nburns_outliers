# itx_nburns_outliers

Code to support ITx Christchurch Presentation: Handling Outliers Properly (Nick Burns, July 2021)

The core theme from the presentation was to really think about why outliers occur and build this into your models, rather than ignoring/removing/truncating them. We've provided two notebooks in this repo to support the presentation. The first notebook runs through a relatively simple application to predict house prices, and demonstrates how we can design our model so that outliers are captured. The second notebook covers a more complex scenario, where outliers arise from unique business processes, and shows how we can develop a mixture model to accurately capture and analyse these different processes. 

To run these notebooks you will need R and RStan installed. The following link will help you get RStan installed: https://github.com/stan-dev/rstan/wiki/RStan-Getting-Started  


We also use R packages: ggplot2 and data.table.

### Background on Bayesian Modelling  

Three of my favourite books on Bayesian Modelling are:  

  - Richard McElreath, Statistical Rethinking (lectures also on youtube) https://xcelab.net/rm/statistical-rethinking/   
  - Gelman *et al* Bayesian Data Analysis 3 (BDA3) http://www.stat.columbia.edu/~gelman/book/  
  - Gelman, Vehtari and Hill, Regression and Other Stories https://www.amazon.com/Regression-Stories-Analytical-Methods-Research/dp/110702398X  
  
The STAN user guide is also fantastic: https://mc-stan.org/docs/2_18/stan-users-guide/index.html  


