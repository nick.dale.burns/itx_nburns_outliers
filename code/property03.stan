data {
  int<lower=1> N;
  int<lower=1> K;
  vector[N] price;
  vector[N] rv;
  vector[N] pc1;
  vector[N] pc2;
  vector[N] pc3;
  vector[N] pc4;
  vector[N] pc5;
  int area[N];
}
parameters {
  real alpha;
  real<lower=0.01> sigma;
  real<lower=0.01> tau;
  vector[K] gamma;
  vector[K] b1;
  vector[K] b2;
  vector[K] b3;
  vector[K] b4;
  vector[K] b5;
}
model {

  vector[N] mu;
  
  sigma ~ normal(0, 2);
  alpha ~ normal(0, 1);
  tau ~ normal(0, 2);
  gamma ~ normal(0, tau);
  b1 ~ normal(0, 0.5);
  b2 ~ normal(0, 0.5);
  b3 ~ normal(0, 0.5);
  b4 ~ normal(0, 0.5);
  b5 ~ normal(0, 0.5);
      
  for (n in 1:N)
  
    mu[n] = rv[n] + alpha + gamma[area[n]] + b1[area[n]]*pc1[n] + b2[area[n]]*pc2[n] + b3[area[n]]*pc3[n] + b4[area[n]]*pc4[n] + b5[area[n]]*pc5[n];
    
  price ~ normal(mu, sigma);
}

