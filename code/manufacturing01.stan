data {
  int N;    
  int reading[N];
}

transformed data {
  int eps = 20;  // Software, hardcoded maximum
}

parameters {
  real<lower=0> lambda[2];         // Poisson rate
  real<lower=0, upper=1> theta;    // Poisson mixture probability
  real<lower=0, upper=1> rho;      // Outage Probability
}

model {

  // Priors
  lambda[1] ~ inv_gamma(5, 15);    // 'Normal' rate roughly around 5 
  lambda[2] ~ gamma(15, 1);        // 'Peak' rate, 12+
  theta ~ beta(100,70);              // Prior for 'normal' operations, approx. 60%
  rho ~ inv_gamma(60, 5);               // prior for 'outages', very low ~1-2%

  // Model
  // Allows for a 'mixture' or normal and peak operations
  // Incorporates the likelihood of 'downtime' when the reading is zero
  
  for (n in 1:N) {
  
    // Normal/peak mixture model
    // This is a truncated model, incorporating the hardcoded max in the software
    real lpdf = log_mix(
      theta,
      poisson_lpmf(reading[n] | lambda[1]) - poisson_lcdf(eps | lambda[1]),
      poisson_lpmf(reading[n] | lambda[2]) - poisson_lcdf(eps | lambda[2])
    );
    
    // Downtime model
    if (reading[n] == 0)
      target += log_mix(rho, 0, lpdf);
      
    else
      target += log(1 - rho) + lpdf;
      
  }
}

