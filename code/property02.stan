data {
  int<lower=1> N;
  int<lower=1> K;
  vector[N] price;
  vector[N] rv;
  vector[N] pc1;
  vector[N] pc2;
  vector[N] pc3;
  vector[N] pc4;
  vector[N] pc5;
  int area[N];
}
parameters {
  real alpha;
  real<lower=0> sigma;
  real<lower=0> tau;
  vector[K] gamma;
  real b1;
  real b2;
  real b3;
  real b4;
  real b5;
}
model {

  vector[N] mu;
  
  sigma ~ normal(0, 5);
  alpha ~ normal(0, 1);
  tau ~ normal(0, 5);
  gamma ~ normal(0, tau);
  b1 ~ normal(0, 1);
  b2 ~ normal(0, 1);
  b3 ~ normal(0, 1);
  b4 ~ normal(0, 1);
  b5 ~ normal(0, 1);
      
  for (n in 1:N)
  
    mu[n] = rv[n] + alpha + gamma[area[n]] + b1*pc1[n] + b2*pc2[n] + b3*pc3[n] + b4*pc4[n] + b5*pc5[n];
    
  price ~ normal(mu, sigma);
}

