data {
  int<lower=1> N;
  vector[N] price;
  vector[N] land;
  vector[N] floorarea;
  vector[N] bed;
  vector[N] bath;
  vector[N] car;
  vector[N] sizeratio;
}
parameters {
  real alpha;
  real b1;
  real b2;
  real b3;
  real b4;
  real b5;
  real b6;
  real<lower=0> sigma;
}
model {
  sigma ~ normal(0, 5);
  alpha ~ normal(0, 5);
  [b1,b2,b3,b4,b5,b6] ~ normal(0, 1);
  
  price ~ normal(alpha + b1*land + b2*floorarea + b3*bed + b4*bath + b5*car + b6*sizeratio, sigma);
}

